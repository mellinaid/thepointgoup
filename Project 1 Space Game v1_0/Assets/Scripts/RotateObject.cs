﻿//@author Linda Lane
//@date 9/5/2016
//@class Script OrbitControl_Script is placed on the sun and on Pluto Focus
// and controls both the rotation speed of both

using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour {


    [Tooltip("Spin Speed is how fast the body rotates on axis. Usual values are 1-2.")]

    public float spinSpeed;

	// Use this for initialization
	void Start () {

	}

	// Fixed Update is called once per frame
	void FixedUpdate () {
		transform.Rotate(0,spinSpeed*Time.deltaTime,0); 
	}
}
