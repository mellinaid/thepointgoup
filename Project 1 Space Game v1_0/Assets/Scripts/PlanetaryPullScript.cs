﻿// GAME 221 JCCC Fall 2016 - Project 1;   PlanetaryPullScript by Linda Lane 
// This script simulates the force of attraction between bodies of mass in space and a
// player character spaceship.
// This script is applied to the heavenly body, aka any space orb.
// To use this script, the orb must have mass and the player must have mass,  scaled as follows
// Mass:  Jupiter = 317, Saturn = 95, Uranus 14, Neptune 17, Earth mass = 1, Venus = 0.8, Mars = .12
// Mercury mass = .055, Player mass = .001 
// The force varies directly with the product of G (gravitational constant) * massOfShip * massOfBody
// and varies inversely with the square of the distance between the ship and body.

//@author Linda Lane
//@date 9/6/2016
//@Class Script PlanetaryPullScript This script simulates the force of attraction between bodies of mass in space and a
// player character spaceship. It uses the mass of the body and the mass of the spaceship to calculate the force.

using UnityEngine;
using System.Collections;

public class PlanetaryPullScript : MonoBehaviour {

   
    [Tooltip("Multiplier for the forces of attraction. Usual values are 1 - 10.")]
    public float gravitationalConstant;         // A multiplier that is applied to the force of attraction            

    //prviate variables
    private float distanceToSpaceship;
    private float forceOfAttraction;
    GameObject player;
    Rigidbody playerRigidbody;
    Rigidbody myRigidbody;

    // Use this for initialization
    void Start ()
    {

        player = GameObject.FindWithTag("Player");
        playerRigidbody = player.GetComponent<Rigidbody>();
        myRigidbody = GetComponent<Rigidbody>();

    }
	
	// Update is called once per frame
	void FixedUpdate () {

      
        Vector3 shipLocation = player.transform.position;     // find the ship's vector3 position
        Debug.Log("Ship is at position x: " + shipLocation.x + " y: " + shipLocation.y + " z: "+ shipLocation.z );


        // Find the direction from the orb to the ship
        Vector3 directionOfSpaceship = shipLocation - transform.position;
        Debug.Log("Direction vector of ship is x: " + directionOfSpaceship.x + " y: " + directionOfSpaceship.y + " z: " + directionOfSpaceship.z);

        // Find the distance between the spaceship and the orb by taking the magnitude of the difference vector
        distanceToSpaceship = Vector3.Distance(player.transform.position, transform.position);
        Debug.Log("Distance to Spaceship is " + distanceToSpaceship);

        //Look at raycast in Scene window:  (start point, how far, what color)
        //Debug.DrawRay(transform.position, directionOfSpaceship.normalized * distanceToSpaceship, Color.green);

        // Force of attraction = (G*mass1*mass2)/(distance*distance)
        forceOfAttraction = (gravitationalConstant * playerRigidbody.mass * myRigidbody.mass)/(distanceToSpaceship * distanceToSpaceship);

        RaycastHit hitOutput;

        // Use the Raycast to find the rigidbody of the Ship
        Physics.Raycast(transform.position, directionOfSpaceship, out hitOutput);

        if (hitOutput.rigidbody == playerRigidbody)     // Make sure we hit the player and nothing else
        {
            hitOutput.rigidbody.AddForceAtPosition(directionOfSpaceship * (-forceOfAttraction), hitOutput.point);
        }
    }
}
