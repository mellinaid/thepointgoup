﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{

    public Transform target;
    public Vector3 offset = -Vector3.forward;
    public float moveSpeed = 1.0f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 wantedPosition = target.position + target.rotation * offset;
        transform.position = Vector3.Lerp(transform.position, wantedPosition, moveSpeed * Time.deltaTime);
    }
}
