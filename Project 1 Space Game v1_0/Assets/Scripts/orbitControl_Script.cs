﻿//@author Linda Lane
//@date 9/5/2016
//@class Script OrbitControl_Script is placed on the planets and the moons
// and controls both the rotation speed and the orbit speed.

using UnityEngine;
using System.Collections;

public class orbitControl_Script : MonoBehaviour {

    [Tooltip("Orbit Speed is how fast a body travels around its orbit. Usual values are 2-5.")]

    public float orbitSpeed;

    [Tooltip("Spin Speed is how fast bodies spin on axis. Usual values are 15-50.")]

    public float spinSpeed;
    [HideInInspector]
    public bool hasBeenHit = false;

	// Use this for initialization
	void Start () {


	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        // Rotate the body on its axis and rotate it around its parent
	
		transform.RotateAround(transform.parent.position, new Vector3(0, 1, 0), orbitSpeed * Time.deltaTime);
		transform.Rotate(0,spinSpeed*Time.deltaTime,0); 
	}
}
