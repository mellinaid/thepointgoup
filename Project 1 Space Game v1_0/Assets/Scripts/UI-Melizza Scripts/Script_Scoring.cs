﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*
 *@author Melizza, Linda, Aiden
 *@date 9/6/2016
 *Class Script Scoring displays the players score, the cargo load, and the 
 *money the player receives.
**/
public class Script_Scoring : MonoBehaviour
{
    [HideInInspector]
    public float score;
    [HideInInspector]
    public float cargo;
    [Tooltip("For if you want the player to lose points for not being fast enough")]
    public int timePenalty = 0;
    [HideInInspector]
    public int cargoBonus = 10000;
    [Tooltip("Lets you set the game timer")]
    public float timer = 60;
    [Tooltip("Displays what the score is")]
    public Text scoreText;
    [Tooltip("Displays how much time the player has left")]
    public Text timerText;
    [Tooltip("Displays how much cargo the player has")]
    public Text cargoText;
    
    //private variables
    bool updateOn = false;

    public void Start()
    {
        GameObject.Find("Score").GetComponent<Text>().text = scoreText.text;
        GameObject.Find("Score").GetComponent<Text>().text = timerText.text;
        GameObject.Find("Score").GetComponent<Text>().text = cargoText.text;

        StartCoroutine(WaitFiveSecs());
    }

    /// <summary>
    /// @author Melizza
    /// it waits five seconds to set the contents of update to true
    /// </summary>
    /// <returns></returns>
    IEnumerator WaitFiveSecs()
    {
        yield return new WaitForSeconds(5.0f);
        updateOn = true;
    }

    /// <summary>
    /// @author Melizza
    /// it waits two seconds to end the game, and take you to highscores
    /// </summary>
    /// <returns></returns>
    IEnumerator EndGame()
    {
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene("06_Highscores");
    }

    void Update()
    {
        if (updateOn == true)
        {
            Timer();
            Scoring();
            score = GameObject.Find("Player").GetComponent<PlayerMove>().totalMoney;
            cargo = GameObject.Find("Player").GetComponent<PlayerMove>().cargo;
        }
    }
     
    /// <summary>
    /// @author Melizza
    /// the timer counts down from the timer variable
    /// until the game runs to zero
    /// </summary>
    void Timer()
    {     
        timer -= Time.deltaTime;
        if(timer <= 0)
        {
            updateOn = false;
            StartCoroutine(EndGame());
        }
    }

    /// <summary>
    /// @author Melizza
    /// This method displays the hud for the player
    /// so they may see their progress in the game
    /// </summary>
    void Scoring()
    {
        if (timer > 0)
            score -= timePenalty;

        scoreText.text = "Money Remaining: $" + score.ToString("n0") + "\nTime Left: " + (int)timer + " seconds" + "\nPackages Left: " + cargo.ToString();
    }
}